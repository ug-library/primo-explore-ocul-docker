//maybe use this Primo space for push to
app.component('prmActionContainerAfter', {
    bindings: {parentCtrl: `<`},
    controller: ['$scope', '$http', '$sce', function ($scope, $http, $sce) {  
        
        alert("action list...");    
        angular.element(document).ready(function () {
            document.getElementById('msg').innerHTML = 'Hello';
        });
    }],    
    template: '<span md-theme="primary" class="prm-primary-bg"><div class="ug-search-eg">action list after</div></span>'
});    

//just some test code for perhaps the push to testcode
angular.element(window.document.body).ready(function ($document) {
    // 
    //el.style.display = 'none';
    //this doesn't seem to trigger on load, it's after?
    //var el_list = document.querySelectorAll('span[translate$="expandresults"]');
    //el_list[0].parentElement.parentElement.parentElement.style.display = 'none';        

    alert("hello");
});

// add in our search term eg
app.component('prmSearchBookmarkFilterAfter',{                  
    //controller: 'prmSearchBookmarkFilterAfterController',
    controller: ['$scope', '$http', '$sce', function ($scope, $http, $sce) {             
        $scope.off_campus = false;            
        $scope.off_campus_url = "";            
        $scope.off_campus_text = "Off Campus Sign Off";

        $http.jsonp("https://app.lib.uoguelph.ca/api/get-ip/",{jsonpCallbackParam: "callback"}).then(function onSuccess(data) {  
            var ip = data.data.ip;                                                                              
            if (
            ((ip.substr(0, 7) == '131.104') &&
                !( // exclude VPN IPs
                    (ip.substr(8,2) == '4.') ||
                    (ip.substr(8,2) == '5.') ||
                    (ip.substr(8,2) == '6.') ||
                    (ip.substr(8,2) == '7.')
                )
            ) || 
            (ip.substr(0, 12) == '209.87.235.1') || // alfred
            (ip.substr(0, 14) == '206.172.38.252') || // ridgetown
            // GH wireless range 64.201.162.67
            (ip.substr(0, 14) == '64.201.162.67') ||
            (ip.substr(0, 14) == '64.201.162.68') ||
            (ip.substr(0, 14) == '64.201.162.69') ||
            (ip.substr(0, 14) == '64.201.162.70') ||
            (ip.substr(0, 14) == '64.201.162.71') ||
            (ip.substr(0, 3) == '10.') // on campus wireless
                    ) {                                        
                //we are on campus
                $scope.off_campus = false;
            }
 
            if ( (ip.substr(0, 14) == '131.104.62.40') ||
                (ip.substr(0, 14) == '131.104.62.35') ||
                (ip.substr(0, 14) == '131.104.62.36') ||
                (ip.substr(0, 14) == '131.104.60.140') ||
                (ip.substr(0, 14) == '131.104.61.131') ||
                (ip.substr(0, 4) == '10.7') || // wireless exception
                (ip.substr(0, 6) == '10.131') || // new VPN IP exclude as of June 19th 2018                                   
                (ip.substr(0, 14) == '131.104.62.54') ) {
                //handle ITS exceptions                    
                // we're off campus
                $scope.off_campus = true;
            }                

            var loggedin = false;
    
            if (location.host.indexOf('subzero.lib.uoguelph.ca') == -1) {
                loggedin = false;
            }
            else {                    
                loggedin = true;                    
            }
            
            // Now evaluate what we found out, and set the appropriate message to display in the page.
            // If the user is on an authorized network, we'll display nothing.                
            if ((!$scope.off_campus) && (!loggedin)) {
                //proxytag for primo
                $scope.off_campus_url = 'https://subzero.lib.uoguelph.ca/login?url="'+window.location.href+'"';
                $scope.off_campus_text = "Off Campus Sign On";
            }		
            else if (loggedin) {
                //primo signout                                        
                $scope.off_campus_url = 'https://auth.lib.uoguelph.ca/Shibboleth.sso/Logout';
                $scope.off_campus_text = "Off Campus Sign Off";
            }
                          
        }).
        catch(function onError(err) {                
            console.log('api error: ' + err);                
        });        
    }],        
    template: `                    
            <div ng-if="off_campus">
            <a class="button-over-dark md-button md-primoExplore-theme md-ink-ripple" id="off-campus-button" aria-label="{{off_campus_text}}" href="{{off_campus_url}}">\
                                    <prm-icon icon-type="svg" svg-icon-set="primo-ui" icon-definition="sign-in"><md-icon md-svg-icon="primo-ui:sign-in" alt="" class="md-primoExplore-theme" aria-hidden="true"><svg id="sign-in_cache18" width="100%" height="100%" viewBox="0 0 24 24" y="192" xmlns="http://www.w3.org/2000/svg" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">\
                                    </svg></md-icon><prm-icon-after parent-ctrl="$ctrl"></prm-icon-after></prm-icon>{{off_campus_text}}</a>
            </div>                                    
        `
    //some template logic gating
    //<div *ngIf="isValid;then content else other_content"></div>
    //<ng-template #content>content here...</ng-template>
    //<ng-template #other_content>other content here...</ng-template>         

    //'<div ng-bind-html="button"></div>'        
    //'<a class="button-over-dark md-button md-primoExplore-theme md-ink-ripple" id="off-campus-button" aria-label="Off Campus Sign On" href="/primo-explore/favorites?vid=GUELPH&amp;lang=en_US&amp;section=search_history">\
    //<prm-icon icon-type="svg" svg-icon-set="primo-ui" icon-definition="sign-in"><md-icon md-svg-icon="primo-ui:sign-in" alt="" class="md-primoExplore-theme" aria-hidden="true"><svg id="sign-in_cache18" width="100%" height="100%" viewBox="0 0 24 24" y="192" xmlns="http://www.w3.org/2000/svg" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">\
    //</svg></md-icon><prm-icon-after parent-ctrl="$ctrl"></prm-icon-after></prm-icon>Off Campus Sign On</a>'
});    

app.controller('prmSearchBookmarkFilterAfterController', ['$scope', '$http', '$sce', function ($scope, $http, $sce) { 
    console.log('in ip controller');                
    $http.jsonp("https://app.lib.uoguelph.ca/api/get-ip/",{jsonpCallbackParam: "callback"}).then(function onSuccess(data) {
        $scope.greeting = data.data.ip;
        console.log('api success: ' + data.data.ip);                    

         //create the new src
         var feed_src = 'tmp text';

         $scope.greeting = $sce.trustAsHtml(feed_src);
    }).
    catch(function onError(err) {
        alert('in ip error catch');  
        console.log('api error: ' + err);
      $scope.data = "Request failed";
    });        
}]);

app.component('prmActionListAfter', {
    bindings: {parentCtrl: `<`},
    template: '<span md-theme="primary" class="prm-primary-bg"><div class="ug-search-eg">action list after</div></span>'
});    

// try the feed burner logic
    //template: '<script src="https://feeds.feedburner.com/UofGuelph/Library?format=sigpro" type="text/javascript"></script>'
    // add in our search term eg
    app.component('ugFeedBurner', {
        bindings: {parentCtrl: `<`},
        controller: 'ugFeedBurnerController',
        template: 'matt tests'
    });    

    app.controller('ugFeedBurnerController', ['$scope', '$http', '$sce', function ($scope, $http, $sce) { 
        console.log('in controller');                
        $http.jsonp("https://app.lib.uoguelph.ca/api/get-ip/?callback=?").then(function onSuccess(data) {
            $scope.greeting = data.data.events;
            console.log('api success: ' + data.data.info);                    

             //create the new src
             var feed_src = 'tmp text';

             $scope.greeting = $sce.trustAsHtml(feed_src);
        }).
        catch(function onError(err) {
            console.log('api error: ' + err);
          $scope.data = "Request failed";
        });        
    }]);

    //just some test code for perhaps the push to testcode
    angular.element(window.document.body).ready(function ($document) {
        // create an injector
        var $injector = angular.injector(['ng']);

        //select the div for action list
       // var action_list = $document[0].div;
        console.log('action list: ' + $document);      

        // create a li element for inserting in action_list
        var newEl = document.createElement('li');
        // use the innerHTML property for inserting HTML content
        // or append a textNode to the p element
        newEl.appendChild(document.createTextNode('Hello World!'));

        // append p as a new child to el
        //action_list.appendChild(newEl);

        // Your function that runs after all DOM is loaded
        alert ("helllloooo");
    });

    template: `                                    
                <div ng-if="off_campus">
                <a class="button-over-dark md-button md-primoExplore-theme md-ink-ripple" id="off-campus-button" aria-label="{{off_campus_text}}" href="{{off_campus_url}}">\
                                        <prm-icon icon-type="svg" svg-icon-set="primo-ui" icon-definition="sign-in"><md-icon md-svg-icon="primo-ui:sign-in" alt="" class="md-primoExplore-theme" aria-hidden="true"><svg id="sign-in_cache18" width="100%" height="100%" viewBox="0 0 24 24" y="192" xmlns="http://www.w3.org/2000/svg" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">\
                                        </svg></md-icon><prm-icon-after parent-ctrl="$ctrl"></prm-icon-after></prm-icon>{{off_campus_text}}</a>
                </div>                                    
            `    

// add in our search term eg
app.component('prmSearchBookmarkFilterAfter',{                  
    //controller: 'prmSearchBookmarkFilterAfterController',
    controller: ['$scope', '$http', '$sce', function ($scope, $http, $sce) {             
        $scope.off_campus = false;            
        $scope.off_campus_url = "";            
        $scope.off_campus_text = "Off Campus Sign Off";

        $http.jsonp("https://app.lib.uoguelph.ca/api/get-ip/",{jsonpCallbackParam: "callback"}).then(function onSuccess(data) {  
            var ip = data.data.ip;                                                                              
            if (
            ((ip.substr(0, 7) == '131.104') &&
                !( // exclude VPN IPs
                    (ip.substr(8,2) == '4.') ||
                    (ip.substr(8,2) == '5.') ||
                    (ip.substr(8,2) == '6.') ||
                    (ip.substr(8,2) == '7.')
                )
            ) || 
            (ip.substr(0, 12) == '209.87.235.1') || // alfred
            (ip.substr(0, 14) == '206.172.38.252') || // ridgetown
            // GH wireless range 64.201.162.67
            (ip.substr(0, 14) == '64.201.162.67') ||
            (ip.substr(0, 14) == '64.201.162.68') ||
            (ip.substr(0, 14) == '64.201.162.69') ||
            (ip.substr(0, 14) == '64.201.162.70') ||
            (ip.substr(0, 14) == '64.201.162.71') ||
            (ip.substr(0, 3) == '10.') // on campus wireless
                    ) {                                        
                //we are on campus
                $scope.off_campus = false;
            }
 
            if ( (ip.substr(0, 14) == '131.104.62.40') ||
                (ip.substr(0, 14) == '131.104.62.35') ||
                (ip.substr(0, 14) == '131.104.62.36') ||
                (ip.substr(0, 14) == '131.104.60.140') ||
                (ip.substr(0, 14) == '131.104.61.131') ||
                (ip.substr(0, 4) == '10.7') || // wireless exception
                (ip.substr(0, 6) == '10.131') || // new VPN IP exclude as of June 19th 2018                                                       
                (ip.substr(0, 14) == '131.104.62.54') ) {
                //handle ITS exceptions                    
                // we're off campus
                $scope.off_campus = true;
            }                

            var loggedin = false;
    
            if (location.host.indexOf('subzero.lib.uoguelph.ca') == -1) {
                loggedin = false;
            }
            else {                    
                loggedin = true;                    
            }
            
            // Now evaluate what we found out, and set the appropriate message to display in the page.
            // If the user is on an authorized network, we'll display nothing.                
            if ((!$scope.off_campus) && (!loggedin)) {
                //proxytag for primo
                $scope.off_campus_url = 'https://subzero.lib.uoguelph.ca/login?url='+window.location.href;
                $scope.off_campus_text = "Library Off Campus Access Sign On";
            }		
            else if (loggedin) {
                //primo signout                                        
                $scope.off_campus_url = 'https://auth.lib.uoguelph.ca/Shibboleth.sso/Logout';
                $scope.off_campus_text = "Sign Off for Library Off Campus Access";
            }            
            
            //on campus...show nothing
            if (!$scope.off_campus) {
                $scope.off_campus_text = "";
                
            }
            $scope.off_campus = true;          
        }).
        catch(function onError(err) {                
            console.log('api error: ' + err);                
        });     
        
        /* This is what the template would be for off campus button
            <div ng-if="off_campus">
            <a class="button-over-dark md-button md-primoExplore-theme md-ink-ripple" id="off-campus-button" aria-label="{{off_campus_text}}" href="{{off_campus_url}}">\
                                    <prm-icon icon-type="svg" svg-icon-set="primo-ui" icon-definition="sign-in"><md-icon md-svg-icon="primo-ui:sign-in" alt="" class="md-primoExplore-theme" aria-hidden="true"><svg id="sign-in_cache18" width="100%" height="100%" viewBox="0 0 24 24" y="192" xmlns="http://www.w3.org/2000/svg" fit="" preserveAspectRatio="xMidYMid meet" focusable="false">\
                                    </svg></md-icon><prm-icon-after parent-ctrl="$ctrl"></prm-icon-after></prm-icon>{{off_campus_text}}</a>
            </div>        
        */
    }],        
    template: ``       
});      

///search on is clicked
// aria-label="Show actions options for this item" 

// document selector via for-
document.querySelector('label[for="prm_mypref.label.my_email"]').parentNode;
document.querySelector('label[for="prm_mypref.label.my_email"]').nextElementSibling;
document.querySelector('label[for="prm_mypref.label.my_email"]').nextElementSibling.value;
"mvanast@uoguelph.ca"


('md-checkbox[aria-label="Add results beyond U of G\'s collection"]')

document.querySelector('input[name="prm_mypref.label.my_email"]').val();

document.querySelector('span[translate="request.holds.request_date"]').parentNode.innerText
document.querySelector('h3[ng-if="::request.firstLineLeft"] > span').value

document.querySelector('h3[ng-if="::request.firstLineLeft"] > span[value="Hamlet"]').parentNode
document.querySelector('h3[ng-if="::request.firstLineLeft"] > span[contains(., "Hamlet")]').parentNode
document.querySelector('h3[ng-if="::request.firstLineLeft"] > span[text()="Hamlet"]').parentNode

document.evaluate("//h1[contains(., 'Hello')]", document, null, XPathResult.ANY_TYPE, null );

//testing insert of request date
var headings = document.evaluate("//span[contains(., 'Hamlet')]", document, null, XPathResult.ANY_TYPE, null );
var this_heading = headings.iterateNext();
var request_div = this_heading.parentNode.parentNode;
var pick_up_p = this_heading.parentNode.parentNode.querySelector('p:last-child');

//Create an attribute to place on the p tag
var att = document.createAttribute("class");       // Create a "class" attribute
att.value = "weak-text";                           // Set the value of the class attribute

//insert request date before pickup
var req_date_span = document.createElement('span');
req_date_span.innerText = "Request Date: 02/02/2020";

var req_date_p = document.createElement('p');
req_date_p.setAttributeNode(att);
req_date_p.append(req_date_span);

request_div.insertBefore(req_date_p, pick_up_p);


var content_div = this_heading.parentNode.parentNode;


span[text()="Hamlet"]

div[contains(., 'Hello')]

Hamlet /

translate="request.holds.request_date"

<div role="alert" aria-live="assertive" layout-align="center center" class="layout-align-center-center">
    <div layout="row" class="bar alert-bar layout-align-center-center layout-row" layout-align="center center">
        <span class="bar-text">Curbside pick-up and mail delivery of items in U of G Library's physical collection are now available. <a class="md-primoExplore-theme" title="Learn more about how to request items" href="https://www.lib.uoguelph.ca/using-library/2019-novel-coronavirus-information/phased-reopening-library-services" >Learn more about how to request items</a>.</span>
    </div>
</div>

The TUG libraries have been collaborating for nearly thirty years. This website provides information on TUG' history, principles, policies, and its key resource, the Annex.

https://ocul-gue-psb.alma.exlibrisgroup.com/mng/login

// Can select format based on:
document.querySelector("[for='prm_almaResourceSharing.format']");

document.querySelector("#copyright-form']");

//digital storytelling out of berkley califoria

//wait 5 seconds for Omni UI to render, then start the timer / attempts - View online takes a long time
function resolveAfter4Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
}

// we want to wait before we trigger the ORL list
async function buildOURListWait() {
    // function value doesn't matter
    var x = await resolveAfter4Seconds(10);                    
}                                  
buildOURListWait();


// wrap OUR creation within an interval attempt - handle UI delays - try 10 times
var our_attempt_count = 0;
var our_attempt_max_count = 50;
var nodeList_created = 0;                
var parent_map = {};
var nodeList_ref = {};

(function buildOURList(){                       
    setTimeout(function() {
        alert("in buildOURList....");
        // Your logic here
        ++our_attempt_count;
        
        var nodeList = document.querySelectorAll('#public-note > a');
        var pNodeList = document.querySelectorAll('#public-note');
        if (nodeList.length > 0) {
            for (var i = 0; i < nodeList.length; i++) {                        
                // add an id or class on link node and parent
                // so we can target the content on API success
                var link_id = "ug-our-link-"+i;
                var parent_id = "p-"+link_id;                                
                
                try {
                    
                    // skip this entry if we've created already
                    //if ( (parent_id in parent_map) || (document.querySelectorAll("[class="+parent_id+"] [class^=ug-our-usage]").length > 0) ) {
                    if ( (document.querySelectorAll("[class="+parent_id+"] [class^=ug-our-usage]").length > 0) ) {
                        //we've already created OUR injection on previous interval for this entry                                        
                        ++nodeList_created;
                        continue;
                    }

                    var node = nodeList[i];
                    var pnode = pNodeList[i];

                    node.setAttribute("id",link_id);
                    pnode.setAttribute("class",parent_id);
                    var info_list = [];

                    var Http = new XMLHttpRequest();
                    var url="https://app.lib.uoguelph.ca/omni/api/get-our-data/?target="+link_id+"&our_url="+encodeURIComponent(node.getAttribute("href"));
                    //make call to get the license info                                            
                    Http.open("GET", url);       
                    Http.send();
                
                    // process the AJAX call when it's ready...won't stop page from loading
                    Http.onreadystatechange = function() {                                
                        if (this.readyState == 4 && this.status==200) {                                    
                            var response_data = JSON.parse(this.responseText);                                               

                            info_list = response_data.our_data;                                            

                            if (info_list.length > 0) {        
                                 //mark as created, so we can remove later
                                 parent_map[parent_id] = "CREATED";
                                 alert("saving nodeList_ref: "+i)
                                 nodeList_ref[i] = response_data;

                                 /*
                                var lic_list = document.createElement('ul');
                                
                                for (var opt in info_list){
                                    //parse the usage
                                    var full_details = info_list[opt];
                                    var usage = "";
                                    var n = full_details.lastIndexOf(" ");
                                    if (n > 0) {
                                        usage = full_details.slice(n+1);
                                        full_details = full_details.slice(0, n+1);                                                
                                    }
                                                                                
                                    //create an UL/li
                                    var line_item = document.createElement('li');
                                    line_item.innerText = full_details;                                            

                                    //create the span
                                    var usage_span = document.createElement('span');
                                    usage_span.innerText = usage;

                                    //determine the class based on usage, hide to start and then show AFTER
                                    var usage_class = "";

                                    if (usage.toLowerCase() == "no") {
                                        usage_class = "ug-our-usage ug-our-no";
                                    }
                                    else if (usage.toLowerCase() == "yes") {
                                    usage_class = "ug-our-usage ug-our-yes";
                                    }
                                    else if (usage.toLowerCase() == "ask") {
                                        usage_class = "ug-our-usage ug-our-ask";
                                    }
                                    
                                    usage_span.setAttribute("class",usage_class);
                                    line_item.setAttribute("class","ug-display-none");
                                    line_item.appendChild(usage_span);

                                    //finalize the li
                                    lic_list.appendChild(line_item);


                                }                                                                        

                                //insert license info before link
                                var license_area = document.createElement('p');
                                license_area.innerText = "License Terms of Use:";
                                license_area.appendChild(lic_list);

                                //Get the target node and parent node
                                var target_link = response_data.target;                                        
                                var p_target_link = "p-"+target_link;                                                                                
                                var tnode = document.querySelector("#"+target_link);
                                var ptnode = document.querySelector("."+p_target_link);
                                ptnode.insertBefore(license_area, tnode);  
                                
                                */
                            }
                        }                        
                    }
                }
                catch(err) {
                    console.log('api error getting Omni OUR data: ' + err);
                };        
            } // end of for loop
        } // end of nodeList  

        // have we reached max tries / no node list or has OUR content been cretaed?
        if ( (our_attempt_count < our_attempt_max_count) && ( (nodeList.length > nodeList_created) || (nodeList.length === 0) )) {                                 
            buildOURList();
        }
        else {
            // We're done with the attempts. We MAY have duplicates that need to be removed
            alert("we're done. We have the OUR response data, now insert once");
            
            var nodeList = document.querySelectorAll('#public-note > a');
            var pNodeList = document.querySelectorAll('#public-note');                            
            if (nodeList.length > 0) {
                alert("nodeList.length is: "+nodeList.length);
                alert("nodeList_ref.length is: "+Object.keys(nodeList_ref).length);

                if (nodeList.length > 0) {
                    for (var i = 0; i < nodeList.length; i++) {  
                        var node1 = nodeList[i];
                        var pnode1 = pNodeList[i];   
                        alert("setting node1 attr");
                        node1.setAttribute("id","a-link-id");
                    }
                }


                // loop through the nodes we have response data for & create
                for (var i in nodeList_ref) {
                    alert("i is: "+parseInt(i));
                    var response_data = nodeList_ref[i];
                    info_list = response_data.our_data;
                    alert("info_list is: "+info_list);
                                       
                    // add an id or class on link node and parent
                    // so we can target the content on API success
                    var link_id = "ug-our-link-"+i;
                    var parent_id = "p-"+link_id;                

                    var node = nodeList[parseInt(i)-1];
                    var pnode = pNodeList[parseInt(i)-1];

                    node.setAttribute("id",link_id);
                    pnode.setAttribute("class",parent_id);    
                    
                    var lic_list = document.createElement('ul');
                                
                    for (var opt in info_list){
                        //parse the usage
                        var full_details = info_list[opt];
                        var usage = "";
                        var n = full_details.lastIndexOf(" ");
                        if (n > 0) {
                            usage = full_details.slice(n+1);
                            full_details = full_details.slice(0, n+1);                                                
                        }
                                                                    
                        //create an UL/li
                        var line_item = document.createElement('li');
                        line_item.innerText = full_details;                                            

                        //create the span
                        var usage_span = document.createElement('span');
                        usage_span.innerText = usage;

                        //determine the class based on usage, hide to start and then show AFTER
                        var usage_class = "";

                        if (usage.toLowerCase() == "no") {
                            usage_class = "ug-our-usage ug-our-no";
                        }
                        else if (usage.toLowerCase() == "yes") {
                        usage_class = "ug-our-usage ug-our-yes";
                        }
                        else if (usage.toLowerCase() == "ask") {
                            usage_class = "ug-our-usage ug-our-ask";
                        }
                        
                        usage_span.setAttribute("class",usage_class);                                        
                        line_item.appendChild(usage_span);

                        //finalize the li
                        lic_list.appendChild(line_item);
                    }
                                                                                                            
                    //insert license info before link
                    var license_area = document.createElement('p');
                    license_area.innerText = "License Terms of Use:";
                    license_area.appendChild(lic_list);

                    //Get the target node and parent node
                    var target_link = response_data.target;                                        
                    var p_target_link = "p-"+target_link;                                                                                
                    var tnode = document.querySelector("#"+target_link);
                    var ptnode = document.querySelector("."+p_target_link);
                    ptnode.insertBefore(license_area, tnode);   

                } //end of nodeList_ref                    
            }
        }
    }, 1000);
})();


/********** START OF UG TOU LINK IN FULL DISPLAY ***************/    
app.component('prmLocationItemAfter', {
    bindings: {parentCtrl: '<'},
    controller: function($scope) {
        this.$onInit = function(){
            {    

                // ******************  START OF UG FEATURE TO ADD LINK INTO TOU DESCIPTION **************/                                          
                // iterate through h4 location item spans, if innerhtml contains a link
                // find the index, find the end of string (end with a .)
                // convert that into a link
                // doing a blanket search on links within spans MAY endup targeting things we don't want
                // only handles 1 url. TOU desc is small, multiple would be messy

                //Just need to wait and Q up behind other single thread JS calls. interval ending condition WILL happen
                var myTOULinkIntervalID = setInterval(addTOULink, 300);
                
                // loop via timer until filter expanded (handle UI delays)
                function addTOULink() {
                    console.log("starting addTOULink");
                    var span_list = document.querySelectorAll("h4.location-item span");

                    for (var i = 0; i < span_list.length; i++) {                        
                        var tou_node = span_list[i];

                        var start_index = 0;
                        //test base protocol
                        if (tou_node.innerHTML.indexOf("http:") > 0) {
                            start_index = tou_node.innerHTML.indexOf("http:");
                        }   

                        // test (and prefer) secure protocol
                        if (tou_node.innerHTML.indexOf("https:") > 0) {
                            start_index = tou_node.innerHTML.indexOf("https:");
                        }                    

                        if (start_index > 0) {
                            //found a url, we'll replace with an <a> tag </a>
                            //Not loanable - follow these steps to access Archival &amp; Special Collections material: https://www.lib.uoguelph.ca/archives/using-archives.
                            //IF it contains a URL, always end it with a .
                            //Go to END and strip off .                                                
                            var end_pos = tou_node.innerHTML.length - 1;
                            var tou_url = tou_node.innerHTML.substring(start_index,end_pos);
                            var tou_href = '<a href="'+tou_url+'">'+tou_url+'</a>';
                            var new_tou = tou_node.innerHTML.replace(tou_url,tou_href);
                            tou_node.innerHTML = new_tou;
                        }
                    }                

                    //end the check / interval AFTER the tou section is rendered.                    
                    if( document.querySelectorAll("h4.location-item span").length > 0) {
                        clearInterval(myTOULinkIntervalID);
                        return;
                    }
                } //end of addTOULink
                /********** END OF UG FEATURE TO ADD LINK INTO TOU DESCIPTION ***************/    
            }
        }; 
    }
});
/********** END OF UG TOU LINK IN FULL DISPLAY ***************/

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GMT0L9R2ZS"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GMT0L9R2ZS');
</script>