FROM node:14-alpine3.15

ENV INSTALL_PATH /app

# Build-base: Package/compilation Essentials
# Git: for potential git-based NPM dependencies
# Python: for node-sass
RUN apk --update add make g++ npm autoconf zlib-dev automake file nasm python3 git

RUN apk add --update --no-cache \
  build-base \
  git 

RUN apk add xvfb

# Install node_modules with yarn
COPY package.json yarn.lock /tmp/
#RUN cd /tmp && yarn install --frozen-lockfile --ignore-optional \
RUN cd /tmp && yarn install --ignore-optional \
  && mkdir -p $INSTALL_PATH \
  && cd $INSTALL_PATH \
  && cp -R /tmp/node_modules $INSTALL_PATH \
  && rm -r /tmp/* && yarn cache clean

WORKDIR $INSTALL_PATH

# Installs packages for any subdirectories
COPY package.json yarn.lock lerna.json ./
COPY ./primo-explore ./primo-explore
RUN yarn lerna bootstrap

COPY . .

EXPOSE 8003 3001

CMD yarn lerna bootstrap && VIEW=${VIEW} PROXY_SERVER=${PROXY_SERVER} yarn start